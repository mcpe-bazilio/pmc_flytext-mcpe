<?php

namespace PMC_FlyText;

use pocketmine\command\PluginCommand;
use pocketmine\event\Listener;
use pocketmine\level\particle\FloatingTextParticle;
use pocketmine\math\Vector3;
use pocketmine\plugin\PluginBase;
use pocketmine\utils\Config;
use pocketmine\utils\TextFormat;


class PMC_FlyText extends PluginBase implements Listener {
	const FILE = "FT.yml";

	/** @var FloatingTextParticle[] $particles Cached FT particles */
	public $particles = [];

	/** @var Config $FT Cached FT file */
	public $FT;

	/** @var Config $config */
	public $config;

	/** @var array $particles2delete Cached FT particles for delete */
	public $particles2delete = [];

	public $cnt = 0;
	public $debug = false;

	public function onLoad(){
		@mkdir($this->getDataFolder());
		$this->saveResource(PMC_FlyText::FILE);
		$this->saveDefaultConfig();
	}

	public function onEnable(){
		$cfg = $this->getConfig()->getAll();
		$this->loadFT();
		$this->getServer()->getPluginManager()->registerEvents(new EventListener($this), $this);
		/** @var PluginCommand $ftCmd */
		$ftCmd = $this->getCommand("ft");
		$ftCmd->setExecutor(new Commands\ft($this));

		$this->scheduleUpdateTexts(10);
		$this->getServer()->getScheduler()->scheduleRepeatingTask(new Tasks\UpdateFTTask($this), 20 * intval($cfg['updatePeriod']));

		$this->getLogger()->info(TextFormat::GREEN . "PMC_FlyText включен!");
	}

	public function onDisable(){
		$this->getLogger()->info("PMC_FlyText выключен.");
	}

	public function getFTconfig($bReload = false){
		if($bReload || !isset($this->FT)){
			$this->FT = new Config($this->getDataFolder() . PMC_FlyText::FILE, Config::YAML);
		}
		return $this->FT;
	}

	public function updateTexts($worldName = null, $FTid = null){
		$this->cnt++;
		if($worldName == null || $FTid == null){
			$worldsTexts = $this->getFTconfig()->getAll();
			foreach($worldsTexts as $worldName => $arFT){
				foreach($arFT as $FTid => $FT){
					$this->drawFT($worldName, $FTid);
				}
			}
		}else{
			$this->drawFT($worldName, $FTid);
		}
		if($this->debug) $this->debugChunkPackets('draw FT');
		$this->getServer()->getScheduler()->scheduleDelayedTask(new Tasks\RemoveFTTask($this), 10);
	}

	public function scheduleUpdateTexts($delta = 0){
		$startAterSec = $delta + intval($this->getConfig()->getAll()['afterEventUpdateDelay']);
		$this->getServer()->getScheduler()->scheduleDelayedTask(new Tasks\UpdateFTTask($this), 20 * $startAterSec);
	}

	public function drawFT($worldName, $FTid){
		$worldsTexts = $this->getFTconfig()->getAll();
		if(!isset($worldsTexts[$worldName])) return null;
		$level = $this->getServer()->getLevelByName($worldName);
		if($level == null) return null;
		$arFT = $worldsTexts[$worldName];
		if(!isset($arFT[$FTid])) return null;
		$FT = $arFT[$FTid];
		if(!isset($this->particles[$worldName])){
			$this->particles[$worldName] = [];
		}
		/* @var FloatingTextParticle[] $pw */
		$pw = &$this->particles[$worldName];


		if(isset($pw[$FTid])){
			//Если частицы удалять сразу, то возможно попадание в один пакет задач на создание и на удаление частицы.
			//В этом случае частица не исчезнет. Поэтому откладываем на 0.5 с задачу по удалению частицы
			$this->particles2delete[] = [
				'level'           => $worldName,
				'particle'        => $pw[$FTid],
				'deleteaftertick' => $this->getServer()->getTick() + 4,
			];
		}
		$coords = explode(',', $FT['coords']);
		$vect = new Vector3($coords[0] + 0.5, $coords[1], $coords[2] - 0.5);
		$title = $FT["title"];

		if($this->debug){
			$title .= ' ' . $this->cnt;
		}

		$pw[$FTid] = new FloatingTextParticle($vect, "", $title . "\n" . $FT["text"]);
		$level->addParticle($pw[$FTid]);
		unset($pw);
	}

	public function debugChunkPackets($caller){
		echo($this->getServer()->getTick() . " >>>>> $caller №" . $this->cnt . "\n");
		foreach($this->getObjProp($this->getServer()->getLevelByName('world'), 'chunkPackets') as $cp0id => $cp1){
			foreach($cp1 as $cp1id => $cp2){
				if($cp2 instanceof \pocketmine\network\protocol\AddEntityPacket) echo("    [$cp1id] Add id: " . $cp2->eid . " \n");
				if($cp2 instanceof \pocketmine\network\protocol\RemoveEntityPacket) echo("         [$cp1id] Remove id: " . $cp2->eid . " \n");
			}
		}
	}

	public function loadFT($bReloadFTConfig = false){
		$worldsTexts = $this->getFTconfig($bReloadFTConfig)->getAll();
		$newFT = [];
		$bNeedSave = false;
		foreach($worldsTexts as $worldName => $arFT){
			$newArFT = [];
			foreach($arFT as $FTid => $ar){
				$flyText = [];
				if(!isset($ar['coords'])){
					$bNeedSave = true;
					continue;
				}
				$coords = explode(',', $ar['coords']);

				if(count($coords) != 3){
					$bNeedSave = true;
					continue;
				}
				$bOK = true;
				foreach($coords as $crd){
					if(!is_numeric($crd)) $bOK = false;
				}
				if(!$bOK){
					$bNeedSave = true;
					continue;
				}
				$flyText['coords'] = '' . $this->compactCoords($coords);

				if(isset($ar["title"]) && trim($ar["title"]) != ''){
					$flyText["title"] = preg_replace('/\r/', '', $ar["title"]);
					if($flyText["title"] != $ar["title"]) $bNeedSave = true;
				}else{
					$flyText["title"] = '';
					$bNeedSave = true;
				}
				$fullText = trim($flyText["title"]);
				if(isset($ar["text"]) && trim($ar["text"]) != ''){
					$flyText["text"] = preg_replace('/\r/', '', $ar["text"]);
					if($flyText["text"] != $ar["text"]) $bNeedSave = true;
				}else{
					$flyText["text"] = '';
					$bNeedSave = true;
				}
				$fullText .= trim($flyText["text"]);
				if(!$fullText){
					$bNeedSave = true;
					continue;
				}
				$newArFT[strtolower($FTid)] = $flyText;
			}
			if(count($newArFT) > 0){
				$newFT[$worldName] = $newArFT;
			}
		}
		if($bNeedSave){
			$this->FT->setAll($newFT);
			$this->FT->save();
			$this->getLogger()->info('§aФайл ' . self::FILE . ' скорректирован и презаписан.');
		}
	}

	public function compactCoord($coord){
		return $this->s_rtrim(number_format(floatval($coord), 1), '.0');
	}

	public function s_rtrim($str, $toCut){
		$pattern = preg_replace('/\./', '\.', $toCut);
		return preg_replace('~' . $pattern . '$~i', '', $str);
	}

	public function compactCoords($coords, $bReturnArray = false){
		$c = is_array($coords) ? $coords : explode(',', $coords);
		$x = $this->compactCoord($c[0]);
		$y = $this->compactCoord($c[1]);
		$z = $this->compactCoord($c[2]);
		return $bReturnArray ? [$x, $y, $z] : "$x,$y,$z";
	}

	public function getObjProp($obj, $propName){
		$reflection = new \ReflectionClass($obj);
		$property = $reflection->getProperty($propName);
		$property->setAccessible(true);
		return $property->getValue($obj);
	}
}
