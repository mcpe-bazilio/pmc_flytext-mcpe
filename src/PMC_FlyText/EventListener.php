<?php
namespace PMC_FlyText;

use pocketmine\event\entity\EntityLevelChangeEvent;
use pocketmine\event\entity\EntityTeleportEvent;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerJoinEvent;
use pocketmine\Player;


class EventListener implements Listener {

	private $plugin;

	function __construct(PMC_FlyText $plugin){
		$this->plugin = $plugin;
	}

	public function onJoin(PlayerJoinEvent $e){
		$this->plugin->scheduleUpdateTexts();
	}


	public function levelChange(EntityLevelChangeEvent $e){
		if($e->getEntity() instanceof Player){
			$this->plugin->scheduleUpdateTexts();
		}
	}

	function EntityTeleport(EntityTeleportEvent $e){
		$this->plugin->scheduleUpdateTexts();
	}


}
