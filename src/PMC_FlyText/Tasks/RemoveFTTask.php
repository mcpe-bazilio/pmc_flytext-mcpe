<?php

namespace PMC_FlyText\Tasks;

use PMC_FlyText\PMC_FlyText;
use pocketmine\level\particle\FloatingTextParticle;
use pocketmine\scheduler\PluginTask;

class RemoveFTTask extends PluginTask {

	public function __construct(PMC_FlyText $Plugin){
		parent::__construct($Plugin);
	}

	public function onRun($tick){
		/* @var PMC_FlyText $p */
		$p = $this->getOwner();

		foreach($p->particles2delete as $id => $p2d){
			/* @var FloatingTextParticle $pp */
			$pp = $p2d['particle'];
			if($p2d['deleteaftertick'] < $tick){
				$level = $p->getServer()->getLevelByName($p2d['level']);
				$level->addChunkPacket($pp->getX() >> 4, $pp->getZ() >> 4, $pp->encode()[0]); //


				//Альтернативный вариант 2 добавления пакета на удаление частицы
				//$pp->setInvisible();
				//$level->addParticle($pp);

				//Альтернативный вариант 3 добавления пакета на удаление частицы
				//$packet = new RemoveEntityPacket();
				//$packet->eid = $p->getObjProp($pp, 'entityId');
				//$level->addChunkPacket($pp->getX() >> 4, $pp->getZ() >> 4, $packet);

				//clear particle obj
				$p2d['particle'] = null;
				unset($p2d['particle']);
				unset($p->particles2delete[$id]);
			}else{
				//echo ('тектик: ' . $tick . ' пропускаю ' . $p2d['deleteaftertick'] . "\n");
			}
		}
		if($p->debug){
			$p->debugChunkPackets('RemoveFTTask');
		}		
	}
}
