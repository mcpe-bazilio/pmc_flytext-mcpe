<?php

namespace PMC_FlyText\Tasks;

use pocketmine\scheduler\PluginTask;
use PMC_FlyText\PMC_FlyText;

class UpdateFTTask extends PluginTask {

	public function __construct(PMC_FlyText $Plugin){
		parent::__construct($Plugin);
	}

	public function onRun($tick){
		/* @var PMC_FlyText $p*/
		$p = $this->getOwner();
		$p->updateTexts();
	}
}
