<?php
/**
 * Created by PhpStorm.
 * User: ss
 * Date: 12.02.2017
 * Time: 14:53
 */

namespace PMC_FlyText\Commands;

use PMC_FlyText\lib\CommandsPMC;
use PMC_FlyText\PMC_FlyText;
use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\level\Position;
use pocketmine\Player;

class ft extends CommandsPMC implements CommandExecutor {

	public function __construct(PMC_FlyText $Plugin){
		parent::__construct($Plugin);
		$this->plugin = $Plugin;
	}

	/**
	 * @param CommandSender $s
	 * @param Command       $cmd
	 * @param string        $label
	 * @param array         $args
	 *
	 * @return bool
	 */
	public function onCommand(CommandSender $s, Command $cmd, $label, array $args){
		if($cmd->getName() != "ft") return false;
		$p = $this->plugin;
		if(count($args) > 3){
			$text = implode(" ", array_slice($args,3));
		}else{
			$text = '';
		}

		$this->normalizeCommandArgs($args);//Убираем пустые ргументы
		if(!isset($args[0])){
			return $this->printHelp($s);
		}

		$subCmd = $subCmdOrig = strtolower($args[0]);
		if(!$this->checkSubCommand($s, $subCmd)) return true;
		if(!$this->canUseSubCommand($s, $subCmd, $subCmdOrig)) return true;

		switch($subCmd){

			case "help":
				return $this->printHelp($s);

			case "list":
				return $this->cmdList($s);

			case "text":
				return $this->cmdText($s, $subCmd, $args, $text);

			case "reload":
				$p->loadFT(true);
				$p->updateTexts();
				return $this->MSG($s, 1, "[✔] Летающие надписи перезагружены");

			case "pos":
				return $this->cmdPos($s, $subCmd, $args);

			case "shift":
				return $this->cmdShift($s, $subCmd, $args);

			case "delete":
				return $this->cmdDelete($s, $subCmd, $args);

			case "tp":
				return $this->cmdTp($s, $subCmd, $args);

			default:
		}

		return true;
	}


	/**
	 * @param CommandSender $s
	 *
	 * @return bool
	 */
	private function cmdList(CommandSender $s){
		$msg = "[✔] ======= Список летающих надписей ======= ";
		$worldsTexts = $this->plugin->getFTconfig()->getAll();
		foreach($worldsTexts as $worldName => $arFT){
			foreach($arFT as $FTid => $FT){
				$coords = $this->plugin->compactCoords($FT['coords'], true);

				$maxL = 30;
				$title = preg_replace('/§\w/im', '', $FT['title']);
				if(mb_strlen($title, 'UTF-8') > $maxL){
					$title = mb_substr($title, 0, $maxL, 'UTF-8');
				}
				$maxL += max(0, $maxL - mb_strlen($title, 'UTF-8'));
				$text = preg_replace('/\r?\n/', ' ', preg_replace('/§\w/', '', $FT['text']));
				if(mb_strlen($text, 'UTF-8') > $maxL){
					$text = mb_substr($text, 0, $maxL - 3, 'UTF-8') . '§7...§b';
				}
				$msg .= "\n§eid: " . $this->fri($FTid, 3) . ', мир ' . $this->frw($worldName, 3) . ', ' . $this->fCrd($coords[0], $coords[1], $coords[2]) .
					'§e, текст: ' . $this->ec($title . '§7 ... §b' . $text, 3, '§b');
			}
		}
		return $this->MSG($s, 1, $msg);
	}

	private function cmdText(CommandSender $s, $subCmd, $args, $text){
		if(!$s instanceof Player){
			return $this->MSG($s, 0, "[✘] Команда /ft $subCmd используется только в игре");
		}
		//$usage = "§aИспользуйте: §e/ft t <id_текста> <N:текст> §a N - номер строки, за которым через двоеточее - текст (1-я строка - заголовок).";
		if(!isset($args[1])){
			return $this->MSG($s, 0, "[✘] Не указан идентификатор надписи. " . $this->getCmdUsage($subCmd, 0, true));
		}
		if(!isset($args[2]) || !preg_match('/^(\d{1,2})$/im', $args[2])){
			return $this->MSG($s, 0, "[✘] Не указан номер строки надписи, в которой размещать новый текст. " . $this->getCmdUsage($subCmd, 0, true));
		}
		$ln = intval($args[2]);

		if(trim($text) == ''){
			return $this->MSG($s, 0, "[✘] Не указан текст надписи. " . $this->getCmdUsage($subCmd, 0, true));
		}
		$text = preg_replace('/\&([0-9abcdefkmolnr])/im', '§\1', $text);

		$p = $this->plugin;

		$maxLines = $p->getConfig()->getAll()['maxLines'];
		if($ln > $maxLines){
			return $this->MSG($s, 0, "[✘] Номер строки не может быть больше $maxLines");
		}

		$pos = $s->getPosition();
		$x = $pos->getX();
		$y = $pos->getY()+1.5;
		$z = $pos->getZ();

		$FTid = strtolower($args[1]);

		$worldsTexts = $p->getFTconfig()->getAll();
		$worldName = $s->getLevel()->getName();
		if(!isset($worldsTexts[$worldName])) $p->FT->set($worldName, []);
		$arFT = $worldsTexts[$worldName];
		if(!isset($arFT[$FTid])){
			$act = 'добавлена';
			$arFT[$FTid] = [];
		}else{
			$act = 'изменена';
		}

		if(!isset($arFT[$FTid]['coords'])){
			$arFT[$FTid]['coords'] = '' . $p->compactCoords([$x, $y, $z]);
		}
		if($ln == 1){
			$arFT[$FTid]['title'] = $text;
		}else{
			if(!isset($arFT[$FTid]['text'])) $arFT[$FTid]['text'] = '';
			$textLines = explode("\n", $arFT[$FTid]['text']);
			for($i = 2; $i <= $maxLines; $i++){
				if($i == $ln){
					$textLines[$i - 2] = $text;
				}else{
					$textLines[$i - 2] = isset($textLines[$i - 2]) ? $textLines[$i - 2] : '';
				}
			}
			$bAdd = false;
			$newText = '';
			for($i = $maxLines; $i >= 2; $i--){
				if(!$bAdd && $textLines[$i - 2] == '') continue;
				$newText = $textLines[$i - 2] . "\n" . $newText;
			}
			$newText = trim($newText, "\n");
			$arFT[$FTid]['text'] = $newText;
		}
		$p->FT->set($worldName, $arFT);
		$p->FT->save();
		$p->loadFT(true);
		$p->updateTexts($worldName, $FTid);
		return $this->MSG($s, 1, "[✔] Летающая надпись " . $this->fri($FTid, 1) . ' ' . $act);
	}

	private function cmdPos(CommandSender $s, $subCmd, $args){

		if($s instanceof Player){
			$usage = $this->getCmdUsage($subCmd, 0, true);
		}else{
			$usage = "Используйте: §e/ft m <id_текста> <x> <y> <z>";
		}

		if(!isset($args[1])){
			return $this->MSG($s, 0, "[✘] Не указан id надписи. " . $usage);
		}


		if(!isset($args[2])){
			if(!$s instanceof Player){
				return $this->MSG($s, 0, "[✘] Для команды в консоли обязательно указывать координаты, куда перемещаете надпись. " . $usage);
			}
			$pos = $s->getPosition();
			$x = $pos->getX();
			$y = $pos->getY();
			$z = $pos->getZ();

			$milt = 1.5;
			$dy = 2;

			$newC = $s->getDirectionVector()->multiply($milt);
			$x += $newC->x;
			$y += $newC->y + $dy;
			$z += $newC->z;
			$where = ' размещена перед вами';
		}else{
			if(!isset($args[3]) || !isset($args[4])){
				return $this->MSG($s, 0, "[✘] " . $usage);
			}
			$x = floatval($args[2]);
			$y = floatval($args[3]);
			$z = floatval($args[4]);
			$where = ' перемещена по координатам ' . $this->fCrd($args[2], $args[3], $args[4]);
		}

		$p = $this->plugin;

		$FTid = strtolower($args[1]);
		$worldsTexts = $p->getFTconfig()->getAll();
		foreach($worldsTexts as $worldName => $arFT){
			if(isset($arFT[$FTid])){

				$arFT[$FTid]['coords'] = '' . $p->compactCoords([$x, $y, $z]);

				$p->FT->set($worldName, $arFT);
				$p->FT->save();
				$p->updateTexts($worldName, $FTid);
				return $this->MSG($s, 1, "[✔] Летающая надпись " . $this->fri($FTid, 1) . $where);
			}
		}
		return $this->MSG($s, 0, "[✘] Отсутствет летающая надпись с id=" . $this->fri($FTid, 0));
	}


	private function cmdShift(CommandSender $s, $subCmd, $args){

		$usage = $this->getCmdUsage($subCmd, 0, true);

		if(!isset($args[1])){
			return $this->MSG($s, 0, "[✘] Не указан id надписи. " . $usage);
		}

		if(!isset($args[2]) || !preg_match('/^[xyz]$/im', $args[2])){
			return $this->MSG($s, 0, "[✘] Не указана координата, по которой смещать надпись. " . $usage);
		}
		$dc = strtoupper($args[2]);

		if(!preg_match('/^(-?[\d\.]+)$/im', $args[3], $matches)){
			return $this->MSG($s, 0, "[✘] Смещение по координате указано неверно. " . $usage);
		}

		$dv = floatval($args[3]);
		$p = $this->plugin;

		$FTid = strtolower($args[1]);
		$worldsTexts = $p->getFTconfig()->getAll();
		foreach($worldsTexts as $worldName => $arFT){
			if(isset($arFT[$FTid])){
				$coords = explode(',', $arFT[$FTid]['coords']);
				switch($dc){
					case "X":
						$coords[0] += floatval($dv);
						break;
					case "Y":
						$coords[1] += floatval($dv);
						break;
					case "Z":
						$coords[2] += floatval($dv);
						break;
				}

				$arFT[$FTid]['coords'] = '' . $p->compactCoords([$coords[0], $coords[1], $coords[2]]);

				$p->FT->set($worldName, $arFT);
				$p->FT->save();
				$p->updateTexts($worldName, $FTid);
				return $this->MSG($s, 1, "[✔] Летающая надпись " . $this->fri($FTid, 1) . " смещена по оси§b $dc §aна§b $dv");
			}
		}
		return $this->MSG($s, 0, "[✘] Отсутствет летающая надпись с id=" . $this->fri($FTid, 0));

	}

	private function cmdDelete(CommandSender $s, $subCmd, $args){
		if(!isset($args[1])) return $this->MSG($s, 0, "[✘] Не указан id надписи. " . $this->getCmdUsage($subCmd, 0, true));
		$p = $this->plugin;
		$FTid = strtolower($args[1]);
		$worldsTexts = $p->getFTconfig()->getAll();
		foreach($worldsTexts as $worldName => $arFT){
			if(isset($arFT[$FTid])){
				unset($arFT[$FTid]);
				$p->FT->set($worldName, $arFT);
				$p->FT->save();
				$p->updateTexts($worldName, $FTid);
				return $this->MSG($s, 1, "[✔] Летающая надпись " . $this->fri($FTid, 1) . ' удалена');
			}
		}
		return $this->MSG($s, 0, "[✘] Отсутствет летающая надпись с id=" . $this->fri($FTid, 0));
	}

	private function cmdTp(CommandSender $s, $subCmd, $args){
		if(!$s instanceof Player) return $this->MSG($s, 0, "[✘] Команда /ft $subCmd используется только в игре");
		if(!isset($args[1])) return $this->MSG($s, 0, "[✘] Не указан id надписи. " . $this->getCmdUsage($subCmd, 0, true));
		$p = $this->plugin;
		$FTid = strtolower($args[1]);
		$worldsTexts = $p->getFTconfig()->getAll();
		foreach($worldsTexts as $worldName => $arFT){
			if(isset($arFT[$FTid])){
				$coords = explode(',', $arFT[$FTid]['coords']);
				$level = $p->getServer()->getLevelByName($worldName);
				if($level == null) return $this->MSG($s, 0, "[✘] Не найден мир " . $this->frw($worldName, 0));

				$pos = new Position($coords[0], $coords[1], $coords[2], $level);

				if($s->teleport($pos)){
					return $this->MSG($s, 1, "[✔] Вы перемещены к летающей надписи " . $this->fri($FTid, 1) . " в точку: " . $this->fCrd($coords[0], $coords[1], $coords[2]));
				}else{
					return $this->MSG($s, 0, "[✘] Телепортация не удалась");
				}
			}
		}
		return $this->MSG($s, 0, "[✘] Отсутствет летающая надпись с id=" . $this->fri($FTid, 0));
	}
}
