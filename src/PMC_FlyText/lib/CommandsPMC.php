<?php
/* PMC lib v 1.0*/
namespace PMC_FlyText\lib;

use PMC_FlyText\PMC_FlyText;
use pocketmine\command\CommandSender;
use pocketmine\plugin\PluginBase;
use pocketmine\utils\TextFormat;

class CommandsPMC extends PluginBase {

	/** @var PMC_FlyText $plugin */
	public $plugin;

	public $mainCommand;

	/** @var array $subCommands Массив подкоманд */
	public $subCommands = [];

	/** @var array $subCommands Массив алиасов */
	public $subCommandAliases = [];

	public function __construct(PluginBase $Plugin){
		$this->plugin = $Plugin;

		foreach($Plugin->getDescription()->getCommands() as $CommandName => $CmdInfo){
			$this->mainCommand = $CommandName;
			$this->subCommands = $CmdInfo['subCommands'];

			foreach($this->subCommands as $subCmdName => $subCmdInfo){
				if(isset($subCmdInfo['aliases'])){
					foreach($subCmdInfo['aliases'] as $alias){
						$this->subCommandAliases[$alias] = $subCmdName;
					}
				}
			}
			break; //обрабатываем только первую команду
		}
	}

	/**
	 * Убирает пустые аргументы
	 *
	 * @param array $args Аргументы команды
	 *
	 * @return array
	 */
	public function normalizeCommandArgs(&$args){
		$args2 = [];
		foreach($args as $key => $arg){
			if(trim($arg) != '') $args2[] = $arg;
		}
		$args = $args2;
		return $args2;
	}

	public function MSG(CommandSender $s, $ResultCode, $msg, $success = true){
		$color = $this->getColorByResultCode($ResultCode);
		$s->sendMessage($color . $msg);
		return $success;
	}

	static function colorAlias(&$usage, $alias, $aliasColor = '§b', $usageColor = '§e'){
		if(!preg_match('%^([a-z0-9§]*/[a-z_0-9]+\s+)([a-z0-9§]+)(.*)$%im', $usage, $matches)) return false;
		$subCmdName = $result = preg_replace('/§\w/i', '', $matches[2]);
		if(preg_match('/^(\w*?)(' . $alias . ')(\w*)$/im', $subCmdName, $m)){
			$subCmdName = $m[1] . $aliasColor . $m[2] . $usageColor . $m[3];
			$usage = $usageColor . $matches[1] . $subCmdName . $matches[3];
			return true;
		}

		$usageNew = $usageColor . $matches[1];
		$residue = $matches[3];
		$aliasLetters = str_split($alias);

		foreach($aliasLetters as $letter){
			if(!preg_match('/^(\w*?)' . $letter . '(\w*)/i', $subCmdName, $matches)) return false;
			$usageNew .= $matches[1] . $aliasColor . $letter . $usageColor;
			$subCmdName = $matches[2];
		}
		$usage = $usageNew . $matches[2] . $residue;
		return true;
	}

	static function getColorByResultCode($ResultCode){
		switch($ResultCode){
			case 0:
				return TextFormat::RED;
			case 1:
				return TextFormat::GREEN;
			case 2:
				return TextFormat::AQUA;
			case 3:
				return TextFormat::YELLOW;
		}
		return TextFormat::AQUA;
	}


	public function getCmdUsage($subCmdName, $ResultCode, $bLabel = false, $bAlias = true){
		if(!isset($this->subCommands[$subCmdName])) return '';
		$sc = $this->subCommands[$subCmdName];
		if(!isset($sc["usage"])) return '';

		$usageColor = '§e';
		$aliasColor = '§b';

		$prefix = ($bLabel ? 'Используйте: ' : '') . $usageColor;
		$usage = $sc["usage"];
		$aliasSuffix = '';
		$endColor = $this->getColorByResultCode($ResultCode);

		if($bAlias){
			if(isset($sc['aliases']) && isset($sc['aliases'][0])){
				$alias = $sc['aliases'][0];
				if(!$this->colorAlias($usage, $alias, $aliasColor, $usageColor)){
					$aliasSuffix = '§f Алиас:' . $usageColor . ' /rg' . $aliasColor . ' ' . $alias;
				}
			}
		}
		return $prefix . $usage . $aliasSuffix . $endColor;
	}

	public function printHelp(CommandSender $s, $success = true){
		$hlp = "§f======§d  " . $this->mainCommand . " help  §f====== §1(плагин разработан специально для §5PLAY-MC.RU§1) §f======";
		foreach($this->subCommands as $subCmdName => $sc){
			if(isset($sc["no-help"]) && $sc["no-help"]) continue;
			$usage = $this->getCmdUsage($subCmdName, 3);
			$descr = isset($sc["description"]) ? $sc["description"] : '';
			$hlp .= "\n§e" . $usage . '§a - ' . $descr;
		}
		$hlp .= "\n§7Выделенные §bбуквы§7 в командах можно использовать как алиасы. Например, §e/cmd §bsc§7 вместо §e/cmd §bs§eub§bc§emd §7";
		$this->MSG($s, 3, $hlp);
		return $success;
	}

	function canUseSubCommand(CommandSender $s, $subCmd, $subCmdOrig){
		if(!$s->hasPermission($this->mainCommand . ".command." . $subCmd)){
			$this->MSG($s, 0, "[✘] У Вас нет прав на выполнение команды " . $this->frc("/" . $this->mainCommand . " $subCmdOrig", 0));
			return false;
		}
		return true;
	}

	function checkSubCommand(CommandSender $s, &$subCmd){
		if(!array_key_exists($subCmd, $this->subCommands)){
			if(!array_key_exists($subCmd, $this->subCommandAliases)){
				$unknownCmdMsg = "[✘] Неизвестная подкоманда " . $this->ec($subCmd, 0) . " " . $this->getCmdUsage('help', 0, true) . " для справки.";
				$this->MSG($s, 0, $unknownCmdMsg);
				return false;
			}
			$subCmd = $this->subCommandAliases[$subCmd];
		}
		return true;
	}

	public function compactCoord($coord){
		return $this->s_rtrim(number_format(floatval($coord), 1), '.0');
	}

	public function s_rtrim($str, $toCut){
		$pattern = preg_replace('/\./', '\.', $toCut);
		return preg_replace('~' . $pattern . '$~i', '', $str);
	}

	/**
	 * Форматирует координаты стандартным образом
	 *
	 * @param      $x
	 * @param      $y
	 * @param      $z
	 * @param bool $label
	 *
	 * @return string
	 */
	public function fCrd($x, $y, $z, $label = false){
		$x = $this->compactCoord($x);
		$y = $this->compactCoord($y);
		$z = $this->compactCoord($z);
		$ret = ($label ? '§dКоординаты ' : '');
		return $ret . "§7x:§6$x §7y:§6$y §7z:§6$z";
	}

	/**
	 * Выделяет цветом участок текста (за выделенным будет цвет в соответствии с $ResultCode)
	 *
	 * @param string $txt   - выделяемый текст
	 * @param int    $ResultCode
	 * @param string $color цвет выделения в формате '§x' . По умолчанию: LIGHT_PURPLE
	 *
	 * @return string
	 */
	public function ec($txt, $ResultCode, $color = null){
		if($color == null) $color = '§d';
		$nextColor = $this->getColorByResultCode($ResultCode);
		return $color . $txt . $nextColor;
	}

	/**
	 * Форматирует команду стандартным образоm
	 *
	 * @param string     $cmd
	 * @param string|int $nextColor
	 *
	 * @return string
	 */
	public function frc($cmd, $nextColor){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return '§e' . $cmd . $nextColor . '';
	}


	/**
	 * Форматирует имя мира стандартным образом
	 *
	 * @param string     $levelName
	 * @param string|int $nextColor
	 * @param string     $q
	 *
	 * @return string
	 */
	public function frw($levelName, $nextColor, $q = ''){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return $nextColor . $q . '§6' . $levelName . $nextColor . $q;
	}

	/**
	 * Форматирует идентификатор сущности образом
	 *
	 * @param string     $id
	 * @param string|int $nextColor
	 * @param string     $q
	 *
	 * @return string
	 */
	public function fri($id, $nextColor, $q = ''){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return $nextColor . $q . '§6' . $id . $nextColor . $q;
	}

}
