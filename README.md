# PMC_FlyText

Make floating texts in your server, in your world, at your conditions.

Version 1.0 // PocketMine API 3.0.0

## Usage
This plugin doesn't have commands or permissions, it only has a simple config file.

### Editing the config file

```

---
# Example config file n. 1
world:                                          # The level where the floating text will be placed
  test:
    coords: 52.0327,68.5067,115.4464            # The X,Y,Z position of the floating text
    title: "§bWelcome to the server! :)"        # The Title inside the floating text
    text: "Any information"                     # The Title inside the floating text
...

```
#### Formatting codes

You can use `§x` formatting codes in the text and `\n` for a new line in the floating text.

```
